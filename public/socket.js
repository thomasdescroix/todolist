$(() => {
    const socket = io(); // Connect to socket.io

    $('#task_form').submit( () => {
        let task = $('#new_task').val();
        socket.emit('add task', task);
        $('#new_task').val('').focus();
        return false; // Blocks the sending of the form
    });

    $(document).on('click', '.delete', (el) => {
        socket.emit('delete task', el.target.parentNode.dataset.index);
    });

    socket.on('update', (tasks) => {
        $('#tasks').empty();
        tasks.forEach( (task, index) => {
            $('#tasks').prepend(`<li data-index="${index}"><a class="delete" href="#">✘</a> ${task}</li>`);
        });
    });
});