<img
  alt="Node.js"
  src="https://nodejs.org/static/images/logo-light.svg"
  width="300"
 />

## Node.js todolist app

The Node.js todolist app teaches the very basics features:

* Events
* Node.js modules and NPM
* Express.js
* Real-time communication with socket.io

## Features

* Show the todolist 
* Add elements to the list via the form
* Delete elements by clicking on the crosses
* Real-time app with socket.io library


## Common setup

Clone the repo and install the dependencies.

```bash
git clone git@gitlab.com:thomasdescroix/todolist.git
cd todolist
```

```bash
npm install
```

To start the express server, run the following

```bash
npm run start
```

Open [http://localhost:3000](http://localhost:3000) in two different browsers and take a look around.
