﻿const express = require('express');
const app = express();
const http = require('http').createServer(app);
const ent = require('ent'); // Allows to block HTML characters
const io = require('socket.io')(http);
const path = require('path');

app.use(express.static(path.join(__dirname, 'public')));

let tasks = [];

app.get('/', (req, res) => {
	res.sendFile(__dirname + '/views/index.html');
})
.use( (req, res) => {
	res.redirect('/');
});

io.on('connection', (socket) =>{
	socket.emit('update', tasks);
	
	socket.on('add task', (task) => {
		task = ent.encode(task);
		tasks.push(task);
		io.emit('update', tasks);		
	});
	
	socket.on('delete task', (index) => {
		tasks.splice(index, 1);
		io.emit('update', tasks);
	});
});

http.listen(3000, () => {
	console.log('listening on *:3000');
});